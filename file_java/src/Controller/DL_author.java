package Controller;

import Data.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Statement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DL_author {
    /**
     * Connect to a sample database
     */
    public static ObservableList<Author> connect() {
        Connection conn = null;
        try {
            // db parameters
            String url = "jdbc:sqlite:C:/Users/axfer/Documents/Axel_Ferreol/CS_2A/ST5/TP_bibliotheque/tp-bibliotheque/data_base2.db";
            // create a connection to the database
            conn = DriverManager.getConnection(url);

            System.out.println("Connection to SQLite has been established.");

            Statement stmt = conn.createStatement();
            String sql = "SELECT id_author, lname, fname, year FROM Author";
            ResultSet rs = stmt.executeQuery(sql);

            ObservableList<Author> v = FXCollections.observableArrayList();

            while (rs.next()) {
                v.add(new Author(rs.getInt("id_author"), rs.getString("lname"), rs.getString("fname"),
                        rs.getString("year")));

            }
            rs.close();
            conn.close();
            System.out.println("Database closed successfully...");
            return v;

        } catch (

        SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return null;

    }

    public static void main(String[] args) {
        ObservableList<Author> v = connect();

        for (Author author : v) {
            System.out.println(author.getId_author() + author.getLname() + author.getFname() + author.getYear());
        }
    }
}