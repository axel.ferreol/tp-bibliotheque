package Controller;

import Data.*;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;
import javafx.scene.control.cell.TextFieldTableCell;

public class DP_gborrow {

    public static VBox display_book() {
        // Creation TableView
        TableView<Borrow> tableView = new TableView<>();
        // Crteation Columns
        TableColumn<Borrow, String> lnameColumn = new TableColumn<>("id_borrow");
        lnameColumn.setCellValueFactory(new PropertyValueFactory<>("id_borrow"));
        TableColumn<Borrow, String> fnameColumn = new TableColumn<>("date_start");
        fnameColumn.setCellValueFactory(new PropertyValueFactory<>("date_start"));
        TableColumn<Borrow, String> titleColumn = new TableColumn<>("date_end");
        titleColumn.setCellValueFactory(new PropertyValueFactory<>("date_end"));
        titleColumn.setCellFactory(TextFieldTableCell.forTableColumn());
        TableColumn<Borrow, String> yearColumn = new TableColumn<>("id_user");
        yearColumn.setCellValueFactory(new PropertyValueFactory<>("id_user"));
        TableColumn<Borrow, String> id_bookColumn = new TableColumn<>("id_book");
        id_bookColumn.setCellValueFactory(new PropertyValueFactory<>("id_book"));

        tableView.getColumns().add(lnameColumn);
        tableView.getColumns().add(fnameColumn);
        tableView.getColumns().add(titleColumn);
        tableView.getColumns().add(yearColumn);
        tableView.getColumns().add(id_bookColumn);

        ObservableList<Borrow> v = DL_gborrow.connect();
        tableView.setItems(v);
        tableView.setEditable(true);
        VBox root = new VBox();

        Button bt = new Button("synchro");
        bt.setOnAction(event -> {
            Update_borrow.connect(v);
        });
        root.getChildren().add(tableView);
        root.getChildren().add(bt);
        return root;
    }

}
