package Controller;

import javafx.collections.ObservableList;
import Data.Book;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;

public class DP_book_availible {

    public static VBox display_book() {
        // Creation TableView
        TableView<Book> tableView = new TableView<>();
        // Crteation Columns
        TableColumn<Book, String> id_bookColumn = new TableColumn<>("Book ID");
        id_bookColumn.setCellValueFactory(new PropertyValueFactory<>("id_book"));
        TableColumn<Book, String> titleColumn = new TableColumn<>("Title");
        titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        TableColumn<Book, String> authorColumn = new TableColumn<>("Author");
        TableColumn<Book, String> firstNameCol = new TableColumn<Book, String>("First Name");
        firstNameCol.setCellValueFactory(new PropertyValueFactory<>("fname_author"));
        TableColumn<Book, String> lastNameCol = new TableColumn<Book, String>("Last Name");
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("lname_author"));
        TableColumn<Book, String> yearColumn = new TableColumn<>("Year");
        yearColumn.setCellValueFactory(new PropertyValueFactory<>("year"));
        TableColumn<Book, Boolean> isSelectedColumn = new TableColumn<>("Selected");
        isSelectedColumn.setCellValueFactory(new PropertyValueFactory<>("isSelected"));
        isSelectedColumn.setCellFactory(col -> {
            return new CheckBoxTableCell<Book, Boolean>();
        });
        authorColumn.getColumns().addAll(firstNameCol, lastNameCol);
        tableView.getColumns().add(id_bookColumn);
        tableView.getColumns().add(authorColumn);
        tableView.getColumns().add(titleColumn);
        tableView.getColumns().add(yearColumn);
        tableView.getColumns().add(isSelectedColumn);

        ObservableList<Book> v = DL_availible.connect();
        tableView.setItems(v);
        tableView.setEditable(true);
        VBox root = new VBox();

        Button bt = new Button("Click to valid");
        bt.setOnAction(event -> {
            Insert_borrow.connect(v);
        });
        root.getChildren().add(tableView);
        root.getChildren().add(bt);

        return root;
    }

}
