package Controller;

import Data.*;
import java.sql.Statement;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Insert_borrow {
    /**
     * Connect to a sample database
     */
    public static void connect(ObservableList<Book> v) {
        Connection conn = null;
        try {
            // db parameters
            String url = "jdbc:sqlite:C:/Users/axfer/Documents/Axel_Ferreol/CS_2A/ST5/TP_bibliotheque/tp-bibliotheque/data_base2.db";
            // create a connection to the database
            conn = DriverManager.getConnection(url);

            System.out.println("Connection to SQLite has been established.");

            for (Book b : v) {
                if (b.isSelected()) {
                    Statement stmt = conn.createStatement();
                    String sql = "INSERT INTO Borrow (date_start,id_user,id_book) VALUES (\"aujourd'hui\",1,"
                            + b.getId_book() + ")";
                    System.out.println(sql);
                    stmt.executeUpdate(sql);
                }
            }

            conn.close();
            System.out.println("Database closed successfully...");

        } catch (

        SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }

    }

    public static void main(String[] args) {
        Book b = new Book(4, "TEST", 1980, 1);
        b.setIsSelected(true);
        ObservableList<Book> v = FXCollections.observableArrayList();
        v.add(b);
        connect(v);
    }
}