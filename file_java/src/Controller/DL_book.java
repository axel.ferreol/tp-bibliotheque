package Controller;

import Data.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Statement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DL_book {
    /**
     * Connect to a sample database
     */
    public static ObservableList<Book> connect() {
        Connection conn = null;
        try {
            // db parameters
            String url = "jdbc:sqlite:C:/Users/axfer/Documents/Axel_Ferreol/CS_2A/ST5/TP_bibliotheque/tp-bibliotheque/data_base2.db";
            // create a connection to the database
            conn = DriverManager.getConnection(url);

            System.out.println("Connection to SQLite has been established.");

            Statement stmt = conn.createStatement();
            String sql = "SELECT id_book, title, year, id_author FROM Book";
            ResultSet rs = stmt.executeQuery(sql);

            ObservableList<Book> v = FXCollections.observableArrayList();

            while (rs.next()) {
                v.add(new Book(rs.getInt("id_book"), rs.getString("title"), rs.getInt("year"), rs.getInt("id_author")));

            }
            rs.close();
            conn.close();
            System.out.println("Database closed successfully...");
            return v;

        } catch (

        SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return null;

    }

    public static void main(String[] args) {
        ObservableList<Book> v = connect();

        for (Book book : v) {
            System.out.println(book.getId_book() + book.getTitle() + book.getYear());
        }
    }
}