package Controller;

import Data.*;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;

public class DP_user {

    public static VBox display_book(int id) {
        // Creation TableView
        TableView<User> tableView = new TableView<>();
        // Crteation Columns
        TableColumn<User, String> lnameColumn = new TableColumn<>("id_user");
        lnameColumn.setCellValueFactory(new PropertyValueFactory<>("id_user"));
        TableColumn<User, String> fnameColumn = new TableColumn<>("fname");
        fnameColumn.setCellValueFactory(new PropertyValueFactory<>("fname"));
        TableColumn<User, String> titleColumn = new TableColumn<>("lname");
        titleColumn.setCellValueFactory(new PropertyValueFactory<>("lname"));

        tableView.getColumns().add(lnameColumn);
        tableView.getColumns().add(fnameColumn);
        tableView.getColumns().add(titleColumn);

        ObservableList<User> v = DL_user.connect();
        tableView.setItems(v);
        tableView.setEditable(true);
        VBox root = new VBox();

        root.getChildren().add(tableView);

        return root;
    }

}
