package Controller;

import Data.*;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;

public class DP_book {

    public static VBox display_book() {
        // Creation TableView
        TableView<Book> tableView = new TableView<>();
        // Creation Columns
        TableColumn<Book, String> lnameColumn = new TableColumn<>("id_book");
        lnameColumn.setCellValueFactory(new PropertyValueFactory<>("id_book"));
        TableColumn<Book, String> titleColumn = new TableColumn<>("Title");
        titleColumn.setCellValueFactory(new PropertyValueFactory<>("title"));
        TableColumn<Book, String> yearColumn = new TableColumn<>("Year");
        yearColumn.setCellValueFactory(new PropertyValueFactory<>("year"));

        TableColumn<Book, String> authorColumn = new TableColumn<>("Author");
        TableColumn<Book, String> firstNameCol = new TableColumn<Book, String>("First Name");
        firstNameCol.setCellValueFactory(new PropertyValueFactory<>("fname_author"));
        TableColumn<Book, String> lastNameCol = new TableColumn<Book, String>("Last Name");
        lastNameCol.setCellValueFactory(new PropertyValueFactory<>("lname_author"));

        authorColumn.getColumns().addAll(firstNameCol, lastNameCol);
        tableView.getColumns().add(lnameColumn);
        tableView.getColumns().add(authorColumn);
        tableView.getColumns().add(titleColumn);
        tableView.getColumns().add(yearColumn);

        tableView.setItems(DL_book.connect());
        tableView.setEditable(false);
        VBox root = new VBox();

        root.getChildren().add(tableView);

        return root;
    }

}
