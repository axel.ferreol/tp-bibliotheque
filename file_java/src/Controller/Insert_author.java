package Controller;

import Data.*;
import java.sql.Statement;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Insert_author {
    /**
     * Connect to a sample database
     */
    public static void connect(ObservableList<Author> v) {
        Connection conn = null;
        try {
            // db parameters
            String url = "jdbc:sqlite:C:/Users/axfer/Documents/Axel_Ferreol/CS_2A/ST5/TP_bibliotheque/tp-bibliotheque/data_base2.db";
            // create a connection to the database
            conn = DriverManager.getConnection(url);

            System.out.println("Connection to SQLite has been established.");

            for (Author b : v) {

                Statement stmt = conn.createStatement();
                String sql = "INSERT INTO Author (lname,fname,year) VALUES (\"" + b.getLname() + "\",\"" + b.getFname()
                        + "\",\"" + b.getYear() + "\")";
                System.out.println(sql);
                stmt.executeUpdate(sql);

            }

            conn.close();
            System.out.println("Database closed successfully...");

        } catch (

        SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }

    }

    public static void main(String[] args) {
        Author b = new Author(4, "Ferreol", "Axel", "1980");
        ObservableList<Author> v = FXCollections.observableArrayList();
        v.add(b);
        connect(v);
    }
}