package Controller;

import Data.Author;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.VBox;

public class DP_author_add {

    public static VBox display_author() {
        // Creation TableView
        TableView<Author> tableView = new TableView<>();
        // Crteation Columns

        TableColumn<Author, String> lnameColumn = new TableColumn<>("Last name");
        lnameColumn.setCellValueFactory(new PropertyValueFactory<>("lname"));
        lnameColumn.setCellFactory(TextFieldTableCell.forTableColumn());

        TableColumn<Author, String> fnameColumn = new TableColumn<>("First name");
        fnameColumn.setCellValueFactory(new PropertyValueFactory<>("fname"));
        fnameColumn.setCellFactory(TextFieldTableCell.forTableColumn());

        TableColumn<Author, String> yearColumn = new TableColumn<>("Year");
        yearColumn.setCellValueFactory(new PropertyValueFactory<>("year"));
        yearColumn.setCellFactory(TextFieldTableCell.forTableColumn());

        tableView.getColumns().add(lnameColumn);
        tableView.getColumns().add(fnameColumn);
        tableView.getColumns().add(yearColumn);

        ObservableList<Author> v = FXCollections.observableArrayList();
        v.add(new Author(0, null, null, "0"));
        tableView.setItems(v);
        tableView.setEditable(true);
        VBox root = new VBox();

        Button bt = new Button("Click to valid");
        bt.setOnAction(event -> {
            Insert_author.connect(v);
        });
        root.getChildren().add(tableView);
        root.getChildren().add(bt);

        return root;
    }

}
