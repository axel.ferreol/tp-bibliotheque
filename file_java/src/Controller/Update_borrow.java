package Controller;

import Data.*;
import javafx.collections.ObservableList;
import java.sql.Statement;
import java.sql.Connection;
import java.sql.DriverManager;

import java.sql.SQLException;

public class Update_borrow {
    /**
     * Connect to a sample database
     */
    public static void connect(ObservableList<Borrow> v) {

        Connection conn = null;
        try {

            // Borrow borrow = v.get(0);
            // db parameters
            String url = "jdbc:sqlite:C:/Users/axfer/Documents/Axel_Ferreol/CS_2A/ST5/TP_bibliotheque/tp-bibliotheque/data_base2.db";
            // create a connection to the database
            conn = DriverManager.getConnection(url);

            System.out.println("Connection to SQLite has been established.");

            for (Borrow b : v) {
                Statement stmt = conn.createStatement();
                String sql = "UPDATE Borrow SET date_end = \"" + b.getDate_end() + "\" WHERE (Borrow.id_borrow ="
                        + b.getId_borrow() + ")";
                System.out.println(sql);
                stmt.executeUpdate(sql);
            }

            conn.close();
            System.out.println("Database closed successfully...");

        } catch (

        SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }

    }

    public static void main(String[] args) {

        ObservableList<Borrow> v = DL_gborrow.connect();
        connect(v);
    }
}