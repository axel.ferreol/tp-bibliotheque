package Controller;

import Data.*;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.sql.Statement;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

public class DL_user {
    /**
     * Connect to a sample database
     */
    public static ObservableList<User> connect() {
        Connection conn = null;
        try {
            // db parameters
            String url = "jdbc:sqlite:C:/Users/axfer/Documents/Axel_Ferreol/CS_2A/ST5/TP_bibliotheque/tp-bibliotheque/data_base2.db";
            // create a connection to the database
            conn = DriverManager.getConnection(url);

            System.out.println("Connection to SQLite has been established.");

            Statement stmt = conn.createStatement();
            String sql = "SELECT id_user, lname, fname FROM User";
            ResultSet rs = stmt.executeQuery(sql);

            ObservableList<User> v = FXCollections.observableArrayList();

            while (rs.next()) {
                v.add(new User(rs.getInt("id_user"), rs.getString("lname"), rs.getString("fname")));

            }
            rs.close();
            conn.close();
            System.out.println("Database closed successfully...");
            return v;

        } catch (

        SQLException e) {
            System.out.println(e.getMessage());
        } finally {
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
        return null;

    }

    public static ObservableList<String> getlnamelist() {
        ObservableList<String> lname_list = FXCollections.observableArrayList();
        for (User user : DL_user.connect()) {
            lname_list.add(user.getLname());
        }
        return lname_list;

    }

    public static void main(String[] args) {
        ObservableList<User> v = connect();

        for (User user : v) {
            System.out.println(user.getId_user() + user.getLname() + user.getFname());
        }
    }
}