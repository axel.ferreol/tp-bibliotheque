package Controller;

import Data.Author;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.VBox;

public class DP_author {

    public static VBox display_author() {
        // Creation TableView
        TableView<Author> tableView = new TableView<>();
        // Crteation Columns
        TableColumn<Author, String> lnameColumn = new TableColumn<>("Last name");
        lnameColumn.setCellValueFactory(new PropertyValueFactory<>("lname"));

        TableColumn<Author, String> fnameColumn = new TableColumn<>("First name");
        fnameColumn.setCellValueFactory(new PropertyValueFactory<>("fname"));

        tableView.getColumns().add(lnameColumn);
        tableView.getColumns().add(fnameColumn);

        ObservableList<Author> v = DL_author.connect();
        tableView.setItems(v);
        tableView.setEditable(false);

        VBox root = new VBox();
        root.getChildren().add(tableView);

        return root;
    }

}
