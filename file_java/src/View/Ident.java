package View;

import Controller.*;
import Data.*;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Ident {

    public static Scene display(Stage primaryStage) {
        VBox root = new VBox();

        // Creation button
        Button button_su = new Button("Librarian");

        Label label = new Label("Select User");

        ComboBox<String> combobox_user = new ComboBox<>();
        combobox_user.setItems(DL_user.getlnamelist());
        combobox_user.setOnAction(event -> {
            int id = User.getid_user((String) combobox_user.getValue());
            System.out.println(id);
            new Client(primaryStage, id).display();
        });

        root.getChildren().add(button_su);
        root.getChildren().add(label);
        root.getChildren().add(combobox_user);
        // SetAction
        button_su.setOnAction((e) -> {
            new Superuser(primaryStage).display();

        });
        return new Scene(root, 400, 400);

    }
}
