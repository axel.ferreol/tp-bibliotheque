package View;

import Controller.DP_author_add;
import Controller.DP_gborrow;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;

public class Superuser extends Client {
    public Superuser(Stage primaryStage) {
        super(primaryStage, 0);

    }

    public void load_menubar() {
        super.load_menubar();

        Menu menu_superuser = new Menu("Super user Actions");
        this.menubar.getMenus().add(menu_superuser);

        MenuItem menuItem_gborrowed = new MenuItem("All borrowed books");
        menu_superuser.getItems().add(menuItem_gborrowed);
        menuItem_gborrowed.setOnAction(e -> {
            this.content = DP_gborrow.display_book();
            this.display();
        });

        MenuItem menuItem_author = new MenuItem("Add author");
        menu_superuser.getItems().add(menuItem_author);
        menuItem_author.setOnAction(e -> {
            this.content = DP_author_add.display_author();
            this.display();
        });
    }

}