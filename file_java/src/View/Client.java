package View;

import Controller.*;
import javafx.scene.input.KeyCombination;
import javafx.scene.layout.VBox;
import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.stage.Stage;
import javafx.scene.Scene;

public class Client {
    // Attributs
    Stage primaryStage;
    int id;
    VBox content = new VBox();
    MenuBar menubar = new MenuBar();

    // Methods
    public Client(Stage primaryStage, int id) {
        this.primaryStage = primaryStage;
        this.id = id;
    }

    public void display() {
        // Creation VBox

        VBox root = new VBox();
        this.load_menubar();
        root.getChildren().add(this.menubar);
        root.getChildren().add(this.content);
        this.primaryStage.setScene(new Scene(root, 400, 400));
    }

    public void load_menubar() {
        // Create MenuBar
        MenuBar menuBar = new MenuBar();
        this.menubar = menuBar;
        // Create Menus
        Menu menu_parametres = new Menu("Parameters");
        Menu menu_selection = new Menu("Actions");
        menuBar.getMenus().addAll(menu_parametres, menu_selection);
        // Create MenuItems

        MenuItem menuItem_id = new MenuItem("Log-out");
        menu_parametres.getItems().add(menuItem_id);
        menuItem_id.setOnAction(e -> {
            this.primaryStage.setScene(Ident.display(this.primaryStage));
        });
        menuItem_id.setAccelerator(KeyCombination.keyCombination("Ctrl+X"));

        MenuItem menuItem_author = new MenuItem("All authors");
        menu_selection.getItems().add(menuItem_author);
        menuItem_author.setOnAction(e -> {
            this.content = DP_author.display_author();
            this.display();
        });

        MenuItem menuItem_book = new MenuItem("All books");
        menu_selection.getItems().add(menuItem_book);
        menuItem_book.setOnAction(e -> {
            this.content = DP_book.display_book();
            this.display();
        });

        MenuItem menuItem_borrowed = new MenuItem("My borrowed books");
        menu_selection.getItems().add(menuItem_borrowed);
        menuItem_borrowed.setOnAction(e -> {
            this.content = DP_borrow.display_book(this.id);
            this.display();
        });

        MenuItem menuItem_book_availible = new MenuItem("All books availible");
        menu_selection.getItems().add(menuItem_book_availible);
        menuItem_book_availible.setOnAction(e -> {
            this.content = DP_book_availible.display_book();
            this.display();
        });

    }
}
