package Data;

import Controller.DL_user;
import javafx.collections.ObservableList;

public class User {
    int id_user;
    String lname;
    String fname;

    public User(int id_user, String lname, String fname) {
        this.id_user = id_user;
        this.lname = lname;
        this.fname = fname;
    }

    public int getId_user() {
        return this.id_user;
    }

    public String getLname() {
        return this.lname;
    }

    public String getFname() {
        return this.fname;
    }

    public static int getid_user(String lname) {
        ObservableList<User> v = DL_user.connect();
        for (User user : v) {
            if (user.getLname().equals(lname)) {
                return user.getId_user();
            }
        }
        return 0;
    }

}
