package Data;

import javafx.beans.property.StringProperty;
import javafx.beans.property.SimpleStringProperty;

public class Borrow {
    int id_borrow;
    String date_start;
    StringProperty date_end;
    int id_user;
    int id_book;

    public Borrow(int id_borrow, String date_start, String date_end, int id_user, int id_book) {
        this.id_borrow = id_borrow;
        this.date_start = date_start;
        this.date_end = new SimpleStringProperty();
        this.setDate_end(date_end);
        this.id_user = id_user;
        this.id_book = id_book;

    }

    public int getId_borrow() {
        return this.id_borrow;
    }

    public String getDate_start() {
        return this.date_start;
    }

    public String getDate_end() {
        return this.date_end.get();
    }

    public void setDate_end(String date_end) {
        this.date_end.set(date_end);
    }

    public StringProperty date_endProperty() {
        return this.date_end;
    }

    public int getId_user() {
        return this.id_user;
    }

    public int getId_book() {
        return this.id_book;
    }
}
