package Data;

import Controller.*;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Book {
    int id_book;
    StringProperty title;
    int year;
    int id_author;
    BooleanProperty selected;

    public Book(int id_book, String title, int year, int id_author) {
        this.id_book = id_book;
        this.title = new SimpleStringProperty();
        this.setTitle(title);
        this.year = year;
        this.id_author = id_author;
        this.selected = new SimpleBooleanProperty(false);

    }

    public int getId_book() {
        return this.id_book;
    }

    public String getTitle() {
        return title.get();
    }

    public void setTitle(String title) {
        this.title.set(title);
    }

    public StringProperty titleProperty() {
        return title;
    }

    public int getYear() {
        return this.year;
    }

    public boolean isSelected() {
        return selected.get();
    }

    public BooleanProperty isSelectedProperty() {
        return selected;
    }

    public void setIsSelected(boolean isSelected) {
        this.selected.set(isSelected);
    }

    public Author get_author() {
        for (Author author : DL_author.connect()) {
            if (author.getId_author() == this.id_author) {
                return author;
            }
        }
        return null;
    }

    public String getFname_author() {
        return this.get_author().getFname();
    }

    public String getLname_author() {
        return this.get_author().getLname();
    }
}
