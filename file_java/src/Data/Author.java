package Data;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Author {
    int id_author;
    StringProperty lname;
    StringProperty fname;
    StringProperty year;

    public Author(int id_author, String lname, String fname, String year) {
        this.id_author = id_author;
        this.lname = new SimpleStringProperty();
        this.setLname(lname);
        this.fname = new SimpleStringProperty();
        this.setFname(fname);
        this.year = new SimpleStringProperty();
        this.setYear(year);

    }

    public String getLname() {
        return lname.get();
    }

    public void setLname(String lname) {
        this.lname.set(lname);
    }

    public StringProperty lnameProperty() {
        return lname;
    }

    public String getFname() {
        return fname.get();
    }

    public void setFname(String fname) {
        this.fname.set(fname);
    }

    public StringProperty fnameProperty() {
        return fname;
    }

    public String getYear() {
        return year.get();
    }

    public void setYear(String year) {
        this.year.set(year);
    }

    public StringProperty yearProperty() {
        return year;
    }

    public int getId_author() {
        return this.id_author;
    }

}
